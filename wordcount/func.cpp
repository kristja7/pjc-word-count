#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <filesystem>
#include <dirent.h>
#include <chrono>

using namespace std;




class Func{
public:

    string file;

    //starts program, prints available files, commands
    void run(){
        cout << "Word Count\n";
        printCommands();
        auto a = true;
        while(a) {
            cout << "Available files:\n";
            getFiles("../Texts/");
            cout << "\nEnter file name\n";
            string in;
            getline(cin, in);
            processInFile(in);

            if (!file.empty()) {
                printCommands();
                cout << "Enter command\n";
                getline(cin, in);
                if (in == "cw") {
                    printCountWords();
                } else if (in == "wf") {
                    printSortedFreq();
                } else if (in == "help") {
                    printCommands();
                    a = false;
                } else if (in == "test") {
                    test();
                } else if (in == "q") {
                    a = false;
                } else {
                    cout << "unknown command\n";
                }
            }
        }
    }


    void processInFile(string a){
        struct dirent *entry;
        DIR *dir = opendir("../Texts/");

        if (dir == NULL) {
            return;
        }
        while ((entry = readdir(dir)) != NULL) {
            if(entry->d_name == a){
                setFile(a);
                cout << "file set\n";
            }
        }
        closedir(dir);
        if(file != a){
            cout << "No such file found\n";
            file = "";
        }

    }
    //runs timed test for single threaded word count
    void test(){
        int avg = 0;
        for(int i = 1; i <6 ; i++) {
            auto start = std::chrono::high_resolution_clock::now();
            countWords();
            auto end = std::chrono::high_resolution_clock::now();
            (end - start).count();
            auto time1 = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            cout <<i << ". time = " << time1 << "ms\n";
            avg += time1;
        }
        cout << "avg time = " << avg/5 <<"ms\n";
    }

    //prints all available commands
    void printCommands(){
        cout <<"-----------------------------------------------\n";
        cout << "Commands: \n";
        cout << "cw \t count words\n";
        cout << "wf \t word frequency\n";
        cout << "test \t time test\n";
        cout << "help \t available commands\n";
        cout << "q \t quit\n";
        cout <<"-----------------------------------------------\n";
    }



    //returns text from selected file
    string getText() {
        string myText;
        string fullText;
        string path = "../Texts/" + file;

        ifstream MyReadFile(path);

        while (getline(MyReadFile, myText)) {
            fullText.append(myText);
        }
        MyReadFile.close();
        return fullText;
    }

    //sets file to work with
    void setFile(string s){
        file = s;
    }

    //returns available files
    void getFiles(const char *path) {
        struct dirent *entry;
        DIR *dir = opendir(path);

        if (dir == NULL) {
            return;
        }
        while ((entry = readdir(dir)) != NULL) {
            cout << entry->d_name << endl;
        }
        closedir(dir);
    }

    //returns number of words in file
    int countWords(){
        auto str = getText();
        int numspaces = 0;
        char nextChar;
        for (int i=0; i<int(str.length()); i++)
        {
            if (isspace(str[i]))
                numspaces++;
        }
        return numspaces + 1;
    }

    //prints number of words in file
    void printCountWords(){
        auto cnt = countWords();
        cout << "\nThere are " << cnt << " words in this string.\n";
    }

    //returns pairs of words and their count
    vector<pair<int, string>> wordFreqency(){
        auto str = getText();
        vector<pair<int, string >> v;
        string word;
        bool flag;
        for(int i = 0; i < (str.length() + 1); i++){
            if(isspace(str[i]) || i == str.length()){
                auto it = v.begin();
                flag = true;
                while(it != v.end()){
                    if(it->second == word){
                        it->first++;
                        flag = false;
                        break;
                    }
                    it++;
                }
                if(flag) {
                    v.push_back(make_pair(1, word));
                }
                word.clear();
            }
            else {
                word.push_back(str[i]);
            }
        }
        return v;
    }


    //prints sorted words in file and their frequency
    void printSortedFreq(){
        cout << "Word frequency:\n";
        auto v = wordFreqency();
        sort(v.begin(), v.end());
        for(auto a: v){
            cout << a.second << " " << a.first <<"x\n";
        }
    }



private:

};
